import datetime
import os
import time
import timeit
import json


import plexapi.audio
from plexapi.server import PlexServer
from plexapi.myplex import MyPlexAccount
from plexapi.audio import Album

plex = None
plex_config = {}


def init_plex(config_file_path):
    global plex_config
    global plex

    plex_config = _read_config_file(config_file_path)
    try:
        plex = PlexServer(plex_config.get('PLEX_URL'), plex_config.get('PLEX_TOKEN'))
        print("plex login successful width api")
    except:
        try:
            account = MyPlexAccount(plex_config.get('PLEX_USERNAME'), plex_config.get('PLEX_PASSWORD'))
            plex = account.resource(plex_config.get('PLEX_SERVER_NAME')).connect()
            print("plex login successful width username and password")
        except:
            print("unable to connect to plex")
            exit(2)


def _read_config_file(config_file_path):
    if os.path.exists(config_file_path):
        with open(config_file_path, 'r') as f:
            try:
                plex_config = json.load(f)
            except ValueError:
                print("Unable to read external_data")
                exit(2)
    else:
        print("Unable to read external_data")
        exit(2)

    return plex_config


# Get all books from plex server
def get_all_metadata():
    print("get metadata")
    plex_audi_section = plex.library.section(plex_config.get('PLEX_LIB_NAME'))
    books = []

    for author in plex_audi_section.all():
        for album in author.albums():
            dict = album.__dict__
            tracks = {}
            duration = 0

            for i, t in enumerate(album.tracks()):
                last_position = t.viewOffset
                tracks[i] = t.title
                duration += t.duration
                bitrate = t.media[0].__dict__['bitrate']

            dict['track'] = tracks
            dict['duration'] = duration
            dict['bitrate'] = bitrate
            dict['last_position'] = last_position
            if not dict['lastViewedAt']:
                dict['lastViewedAt'] = datetime.datetime(2000, 1, 1)

            books.append(dict)

    return books


# TODO speed this up. maby not loop over all albums and titels
def get_plex_album_by_book(book):
    for author in plex.library.section(plex_config.get('PLEX_LIB_NAME')).all():
        author_dict = author.__dict__
        if author_dict['title'] == book.Author:
            for album in author:
                album_dict = album.__dict__
                if album_dict['title'] == book.Title:
                    return album


def download(book):
    album = get_plex_album_by_book(book)
    print("downloading", album.__dict__['title'])
    file_path = album.download(SAVE_PATH, True)
    return file_path[0]
