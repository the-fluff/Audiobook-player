"""

"""
import json
import os

from tinytag import TinyTag
from plex import get_data
from playsound import playsound
from threading import Thread


LIBRARY_PATH = "/home/oliver/AudiBookPlayer/"
BOOK_PATH = os.path.join(LIBRARY_PATH, "Books")
CONFIG_PATH = os.path.join(LIBRARY_PATH, "Config")
JSON_LIB_PATH = os.path.join(LIBRARY_PATH, "library.json")


class Book:
    def __init__(self, title, author, release_date, series=None, length=None, isbn=None, narrator=None,
                 genres=None, publisher=None, summary=None, rating=None, last_position=None, last_played=None,
                 chapters=None, bitrate=None, filesize=None, plex_key=None, details_key=None, art=None, played=False):
        self.Art = art
        self.Author = author
        self.Bitrate = bitrate
        self.Chapters = chapters
        self.Details_key = details_key
        self.Filesize = filesize
        self.Genres = None  # TODO fix this
        self.ISBN = isbn
        self.Length = length
        self.Narrator = narrator
        self.Played = played
        self.Plex_key = plex_key
        self.Publisher = publisher
        self.Last_position = last_position
        self.Last_played = last_played
        self.Rating = rating
        self.Release_date = release_date
        self.Series = series
        self.Summary = summary
        self.Title = title

    def as_dict(self):
        return {k: v for k, v in self.__dict__.items() if not k.startswith('_')}

    def __str__(self):
        string = f"Title: {self.Title} \n" \
                 f"Author: {self.Author}\n" \
                 f"Series: {self.Series}\n" \
                 f"Length: {self.Length}\n" \
                 f"Narrator: {self.Narrator}\n" \
                 f"Genres: {self.Genres}\n" \
                 f"Release_date: {self.Release_date}\n" \
                 f"Publisher: {self.Publisher}\n" \
                 f"Summary: {self.Summary}\n" \
                 f"Rating: {self.Rating}\n" \
                 f"Last_position: {self.Last_position}\n" \
                 f"Chapters: {self.Chapters}\n" \
                 f"Bitrate: {self.Bitrate}\n" \
                 f"Filesize: {self.Filesize}\n" \
                 f"Plex_key: {self.Plex_key}\n" \
                 f"Art {self.Art}\n"
        return string

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def init_and_parse_plex_book(self, plex_book_dict):
        played_track = False
        if plex_book_dict['last_position'] >= plex_book_dict['duration']:
            played_track = True

        self.Art = None
        self.Author = _find(plex_book_dict['_data'], 'parentTitle')
        self.Bitrate = plex_book_dict['bitrate']
        self.Chapters = None # TODO need to be added
        self.Details_key = plex_book_dict['_details_key']
        self.Filesize = None,  # TODO add this in get metadata
        self.Genres =  None # TODO fix this
        self.ISBN = None # TODO add this in get metadata
        self.Length = plex_book_dict['duration']
        self.Narrator = plex_book_dict['styles'],  # TODO not working
        self.Played = played_track
        self.Plex_key = plex_book_dict['key']
        self.Publisher = plex_book_dict['studio']
        self.Last_position = plex_book_dict['last_position']
        self.Last_played = plex_book_dict['lastViewedAt']
        self.Rating = _find(plex_book_dict['_data'], 'rating')
        self.Release_date = _find(plex_book_dict['_data'], 'originallyAvailableAt')
        self.Series = _find(plex_book_dict['_data'], 'titleSort')
        self.Summary = plex_book_dict['summary']
        self.Title = _find(plex_book_dict['_data'], 'title')

        return self

    def init_and_parse_json(self, json_data):
        print(type(json_data))
        # self.Art = art
        # self.Author = author
        # self.Bitrate = bitrate
        # self.Chapters = chapters
        # self.Details_key = details_key
        # self.Filesize = filesize
        # self.Genres = None  # TODO fix this
        # self.ISBN = isbn
        # self.Length = length
        # self.Narrator = narrator
        # self.Played = played
        # self.Plex_key = plex_key
        # self.Publisher = publisher
        # self.Last_position = last_position
        # self.Last_played = last_played
        # self.Rating = rating
        # self.Release_date = release_date
        # self.Series = series
        # self.Summary = summary
        # self.Title = title

        return self



class Library:
    def __init__(self):
        self.Books = {}

    def remove_book(self, book):
        del self.Books[book.Title]
        #self.Books.remove(book)

    def add_book(self, book):
        self.Books[book.Title] = book

    def print_books(self):
        string = ""
        for b in self.Books.values():
            string += f"Title: {b.Title}\n"
        return string

    def sort_by_title(self):
        # self.Books = sorted(self.Books, key=operator.attrgetter('Title'))
        pass

    def sort_by_author(self):
        # self.Books = sorted(self.Books, key=operator.attrgetter('Author'))
        pass

    def sort_by_release_date(self):
        # self.Books = sorted(self.Books, key=operator.attrgetter('Release_date'), reverse=True)
        pass

    def sort_by_last_played(self):
        # self.Books = sorted(self.Books, key=operator.attrgetter('Last_played'), reverse=True)
        pass

    def sort_by_length(self):
        # self.Books = sorted(self.Books, key=operator.attrgetter('Length'), reverse=True)
        pass

    def read_lib_json_file(self):
        if not os.path.exists(JSON_LIB_PATH):
            raise Exception("No Lib file")
        json_data = []
        with open(JSON_LIB_PATH, "r", encoding="UTF-8") as json_file:
            for line in json_file:
                json_data.append(json.loads(line))

        for data in json_data:
            self.add_book(Book.init_and_parse_json (data))

    def update_json_file(self):
        pass

    def write_lib_to_file(self):
        with open(JSON_LIB_PATH, 'w', encoding="UTF-8") as fp:
            for books in self.Books.values():
                fp.write(json.dumps(books.as_dict(), sort_keys=True, default=str, separators=(',', ': '))+"\n")


def _find(dict, keyword):
    for key, val in dict.items():
        if keyword == key:
            return val


# TODO fix this
def init_book_from_filepath(file_path):
    meta = TinyTag.get(file_path)
    return Book(title=meta.title,
                author=meta.albumartist,
                release_date=meta.year,
                series=meta.album,
                length=meta.duration,
                narrator=meta.composer,
                genres=meta.genre,
                summary=meta.extra,
                bitrate=meta.bitrate,
                filesize=meta.filesize)


# TODO fix this
def list_files_in_library(library_path):
    files = []
    library = Library(None)

    # TODO add all different file types
    for r, d, f in os.walk(library_path):
        for file in f:
            if '.mp3' in file or '.m4b' in file or '.m4a' in file:
                file_path = os.path.join(r, file)
                files.append(file_path)
                book = init_book_from_filepath(file_path)
                library.add_book(book)

    print(library.Books)


if __name__ == "__main__":
    if not os.path.exists(LIBRARY_PATH):
        os.makedirs(LIBRARY_PATH)

    if not os.path.exists(CONFIG_PATH):
        os.makedirs(CONFIG_PATH)

    if not os.path.exists(BOOK_PATH):
        os.makedirs(BOOK_PATH)

    lib = Library()
    try:
        lib.read_lib_json_file()
    except Exception as e:
        print(e)

    plex_config_path = os.path.join(CONFIG_PATH, 'plex_config.json')
    get_data.init_plex(plex_config_path)

    plex_all_data = get_data.get_all_metadata()
    for plex_data in plex_all_data:
        lib.add_book(Book.init_and_parse_plex_book(plex_data))

    lib.update_json_file()
    lib.write_lib_to_file()
